# Lambda DynamoDB Interaction

This project demonstrates how to deploy an AWS Lambda function that interacts with DynamoDB. The function is implemented in Rust, utilizing Cargo for management, and deployed using `cargo-lambda`.

## Deployment

The Lambda function is automatically deployed through GitLab CI/CD upon commits to the `main` branch. The deployment process deploys it to AWS Lambda.

## Invoking the Function

To invoke the deployed Lambda function via the exposed API Gateway endpoint, use the following `curl` command:

```bash
curl -X PUT 'https://y4ud6cyanj.execute-api.us-east-1.amazonaws.com/dev/lambda-dynamo-put' \
-H 'Content-Type: application/json' \
-d '{"table_name": "test", "employer": "mike", "email": "mm3@gmail.com"}'
```

Replace the `employer` and `email` fields with your own values to customize the database entry. If the command succeeds, it will return **{"message":"Item inserted/updated successfully"}**

## Function Execution Success

Below is a screenshot showing the successful execution of the Lambda function and the item insertion in DynamoDB:

![Function Execution Success](dynamo-items.png)