use lambda_runtime::{handler_fn, Context, Error};
use serde_json::{Value, json};
use aws_sdk_dynamodb::{Client, Region, model::AttributeValue};
use aws_config::meta::region::RegionProviderChain;
use serde::Deserialize;

#[derive(Deserialize)]
struct DynamoRequest {
    table_name: String,
    employer: String,
    email: String,
}

async fn function_handler(event: Value, _: Context) -> Result<Value, Error> {
    let request: DynamoRequest = serde_json::from_value(event).map_err(|e| {
        eprintln!("Error parsing event: {:?}", e);
        Error::from(e)
    })?;
    let region_provider = RegionProviderChain::default_provider().or_else(Region::new("us-east-2"));
    let config = aws_config::from_env().region(region_provider).load().await;
    let client = Client::new(&config);

    let mut item = std::collections::HashMap::new();
    item.insert("Employer".to_string(), AttributeValue::S(request.employer.clone()));
    item.insert("Value".to_string(), AttributeValue::S(request.email.clone()));

    match client.put_item()
        .table_name(request.table_name)
        .set_item(Some(item))
        .send().await {
        Ok(_) => Ok(json!({"message": "Item inserted/updated successfully"})),
        Err(e) => {
            eprintln!("DynamoDB error: {:?}", e);
            Err(Error::from(e))
        },
    }
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    lambda_runtime::run(handler_fn(function_handler)).await?;
    Ok(())
}
